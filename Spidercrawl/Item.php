<?php
if (! class_exists('Item')) {
    
    require_once dirname(__FILE__) . '/SpidercrawlAbstract.php';
    require_once dirname(__FILE__) . '/../Scrapy/ScrapyItem.php';

    class Item extends SpidercrawlAbstract
    {

        private $scrapyItemManager;

        private $id;

        private $name;

        private $resource_uri;

        private $spider;

        private $xPath;

        private $meta;

        private $objects;

        /**
         *
         * @param ScrapyItem $scrapyItemManager            
         */
        public function setScrapyItemManager($scrapyItemManager)
        {
            $this->scrapyItemManager = $scrapyItemManager;
        }

        /**
         *
         * @param field_type $id            
         */
        public function setId($id)
        {
            $this->id = $id;
        }

        /**
         *
         * @param field_type $name            
         */
        public function setName($name)
        {
            $this->name = $name;
        }

        /**
         *
         * @param field_type $resource_uri            
         */
        public function setResource_uri($resource_uri)
        {
            $this->resource_uri = $resource_uri;
        }

        /**
         *
         * @param field_type $spider            
         */
        public function setSpider($spider)
        {
            $this->spider = $spider;
        }

        /**
         *
         * @param field_type $xPath            
         */
        public function setXPath($xPath)
        {
            $this->xPath = $xPath;
        }

        /**
         *
         * @param field_type $objects            
         */
        public function setObjects($objects)
        {
            $this->objects = $objects;
        }

        /**
         *
         * @param field_type $meta            
         */
        public function setMeta($meta)
        {
            $this->meta = $meta;
        }

        private function getScrapyItemManager()
        {
            if (! $this->scrapyItemManager) {
                $this->scrapyItemManager = new ScrapyItem();
            }
            return $this->scrapyItemManager;
        }

        /**
         *
         * @return the $id
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * The name consists in a concat btw itemName spiderName and the itemName.
         * This method return only the spiderName
         *
         * @return the $name
         */
        public function getName()
        {
            $names = explode('_', $this->name); // the first string before underscore should contain projectName, the second string, the spiderName and the third one the actual itemName
                                                
            // this condition is here only to allow backward compatibility for those spider which have been created without the projectName_spiderName
            if (! array_key_exists(2, $names)) {
                return $names[0];
            }
            
            return $names[2];
        }

        /**
         * The name consists in a concat btw itemName spiderName and the itemName.
         * This method return the full name concatenated
         *
         * @return the $name
         */
        public function getFullName()
        {
            return $this->name;
        }

        /**
         *
         * @return the $resource_uri
         */
        public function getResource_uri()
        {
            return $this->resource_uri;
        }

        /**
         *
         * @return the $spider
         */
        public function getSpider()
        {
            return $this->spider;
        }

        /**
         *
         * @return the $xPath
         */
        public function getXPath()
        {
            return $this->xPath;
        }
        
        public function getIdSpider(){
            $spider = trim(str_replace('/', ' ',$this->getSpider()));
            $list = explode( ' ',  $spider);
            return end($list);
        }
        
        /*
         * function __construct() { parent::__construct(); $this->scrapy = new \Spidercrawl\Scrapy\Scrapy(); }
         */
      /*  public function setIdItem($idItem)
        {
            $scrapyItem = $this->getScrapyItemManager()->get($idItem);
            if (! $scrapyItem) {
                $this->addError($this->getScrapyItemManager()
                    ->getLastError());
                return false;
            }
            $scrapyItem = json_decode($scrapyItem);
            $this->id = $scrapyItem->id;
            $this->name = $scrapyItem->name;
            $this->resource_uri = $scrapyItem->resource_uri;
            $this->spider = $scrapyItem->spider;
            $this->xPath = $scrapyItem->xPath;
            
            unset($scrapyItem);
        }*/
    }
}