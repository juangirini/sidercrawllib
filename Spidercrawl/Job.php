<?php
if (! class_exists('Job')) {
    
    require_once dirname(__FILE__) . '/SpidercrawlAbstract.php';
    require_once dirname(__FILE__) . '/../Scrapy/ScrapyJob.php';

    class Job extends SpidercrawlAbstract
    {

        private $scrapyJobManager;

        private $end_time;

        private $id;

        private $resource_uri;

        private $spider;

        private $start_time;

        private $state;

        private $status;
        
        /**
	 * @param field_type $end_time
	 */
	public function setEnd_time($end_time) {
		$this->end_time = $end_time;
	}

		/**
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

		/**
	 * @param field_type $resource_uri
	 */
	public function setResource_uri($resource_uri) {
		$this->resource_uri = $resource_uri;
	}

		/**
	 * @param field_type $spider
	 */
	public function setSpider($spider) {
		$this->spider = $spider;
	}

		/**
	 * @param field_type $start_time
	 */
	public function setStart_time($start_time) {
		$this->start_time = $start_time;
	}

		/**
	 * @param field_type $state
	 */
	public function setState($state) {
		$this->state = $state;
	}

		/**
	 * @param field_type $status
	 */
	public function setStatus($status) {
		$this->status = $status;
	}

		private function getScrapyJobManager()
        {
            if (! $this->scrapyJobManager) {
                $this->scrapyJobManager = new \ScrapyJob();
            }
            return $this->scrapyJobManager;
        }
/*
        public function setIdJob($idJob, $idSpider)
        {
            $scrapyJob = $this->getScrapyJobManager()->get($idJob, $idSpider);
            
            if (! $scrapyJob) {
                $this->addError($this->getScrapyJobManager()
                    ->getLastError());
                return false;
            }
            
            $scrapyJob = json_decode($scrapyJob);
            
            $this->end_time = $scrapyJob->end_time;
            $this->id = $scrapyJob->id;
            $this->resource_uri = $scrapyJob->resource_uri;
            $this->idSpider = $scrapyJob->spider;
            $this->start_time = $scrapyJob->start_time;
            $this->state = $scrapyJob->state;
            $this->status = $scrapyJob->status;
            
            unset($scrapyJob);
        }*/

        /**
         *
         * @return the $end_time
         */
        public function getEnd_time()
        {
            return $this->end_time;
        }

        /**
         *
         * @return the $id
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         *
         * @return the $resource_uri
         */
        public function getResource_uri()
        {
            return $this->resource_uri;
        }

        /**
         *
         * @return the $idSpider
         */
        public function getIdSpider()
        {
            return $this->getSpider();
        }

        /**
         *
         * @return the $idSpider
         */
        public function getSpider()
        {
            return $this->spider;
        }

        /**
         *
         * @return the $start_time
         */
        public function getStart_time()
        {
            return $this->start_time;
        }

        /**
         *
         * @return the $state
         */
        public function getState()
        {
            return $this->state;
        }

        /**
         *
         * @return the $status
         */
        public function getStatus()
        {
            return $this->status;
        }

        public function getLog()
        {
            $parameters = array(
                "job" => array(
                    "id" => $this->id
                ),
                "spider" => array(
                    "id" => $this->getIdSpider()
                )
            );
            return json_decode($this->getScrapyJobManager()->log($parameters), true);
        }

        public function getResult($idResult = null)
        {
            $parameters = array(
                "job" => array(
                    "id" => $this->id
                ),
                "spider" => array(
                    "id" => $this->getIdSpider()
                )
            );
            
            if (! is_null($idResult)) {
                $parameters["id"] = $idResult;
            }
            
            return json_decode($this->getScrapyJobManager()->result($parameters), false);
        }
    }
}