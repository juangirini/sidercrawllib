<?php
// namespace Spidercrawl\Spidercrawl;
if (! class_exists('Project')) {

    require_once dirname(__FILE__) . '/SpidercrawlAbstract.php';
    require_once dirname(__FILE__) . '/Spider.php';
    require_once dirname(__FILE__) . '/../Scrapy/ScrapySpider.php';

    class Project extends SpidercrawlAbstract
    {

        private $scrapyProjectManager;

        private $scrapySpiderManager;

        private $name;

        private $id;

        private $resource_uri;

        private $spiders;

        private $meta;
        
        private $objects;

        public function setId($id)
        {
            $this->id = $id;
        }

        public function setMeta($meta)
        {
            $this->meta = $meta;
        }

        public function setObjects($objects)
        {
            $this->objects = $objects;
        }

        public function setName($name)
        {
            $this->name = $name;
        }

        public function setResource_uri($resource_uri)
        {
            $this->resource_uri = $resource_uri;
        }

        private function getScrapySpiderManager()
        {
            if (! $this->scrapySpiderManager) {
                $this->scrapySpiderManager = new ScrapySpider();
            }
            return $this->scrapySpiderManager;
        }

        /**
         *
         * @return the $name
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         *
         * @return the $id
         */
        public function getId()
        {
            return $this->id;
        }
        
        public function getMeta()
        {
            return $this->meta;
        }
        
        public function getObjects()
        {
            return $this->objects;
        }

        function getSpiders()
        {
            $scrapySpiderManager = $this->getScrapySpiderManager();
            $scrapySpiders=$scrapySpiderManager->get(array("project"=>array("id"=>$this->getId())));
            if (! $scrapySpiders) {
                $this->addError($this->getScrapySpiderManager()
                    ->getLastError());
                return false;
            }
            $scrapySpiders = json_decode($scrapySpiders);
            $spiders = array();
            foreach ($scrapySpiders->objects as $spider) {
                $spiders[$spider->id] = $this->getSpider($spider);
            }

            return $spiders;
        }
        
        public function getSpiderNames(){
            $spiders = $this->getSpiders();
            $spiderNames = array();
            foreach ($spiders as $spider){
                $spiderNames[] = $spider->getName();
            }
            return $spiderNames;
        }
     
        function getSpider($object)
        {
        	if(isset($object->id)){
        		$idSpider = $object->id;
        	}elseif(is_int($object)){
        		$idSpider = $object;
        	}else{
        		throw new \Exception('Unsupported format when trying to get a Spider in '.get_class($this).'::getSpider()');
        	}
        	if (! isset($this->spiders[$idSpider])) {
        		if (!($new_spider = new Spider($object))){
        
        			$this->addError("Unable to load the requested Spider.");
        			return false;
        		}
        		$this->spiders[$idSpider] = $new_spider;
        	}
        	return $this->spiders[$idSpider];
        }        

        /**
         *
         * @return the $resource_uri
         */
        public function getResource_uri()
        {
            return $this->resource_uri;
        }

        /**
         * Add a new spider to the project
         *
         * @param Array $parameters
         *            Dictionary containing the spider parameters.
         *            Keys:
         *            - name: String *
         *            - start_domains: array [start_domain_list]
         *            - allowed_domains: array [allowed_domain_regex_list]
         *            - denied_domains: array [denied_domain_regex_list]
         *            - depth: integer
         *            - bot_name: string
         *            - concurrent_requests: integer
         *            - download_delay: integer
         *            - read_robotstxt: boolean
         *            - max_url_crawled: integer (-1 = unlimited)
         *            - url_length_limit: integer
         *            - use_proxies: boolean
         * @return Boolean
         */
        function addSpider($parameters)
        {
            // we should check that the key name exists before playing around with it
            if (! array_key_exists('name', $parameters) || empty($parameters['name'])) {
                $this->addError("No spider name specified.");
                return false;
            } elseif (strpos($parameters['name'], "_") !== false) { // underscore is a reserved character in names, so it shouldn't be allowed
                $this->addError("Underscores are not allowed in the Name field.");
                return false;
            } elseif (strpos($parameters['name'], "-") !== false) { // hyphens are not allowed in spider's names
                $this->addError("Hyphens are not allowed in the Name field.");
                return false;
            }
            
            if(!array_key_exists('start_domains', $parameters) || ! is_array($parameters['start_domains'])){
            	$this->addError("Parameter start_domains should be an array");
            	return false;
            }
            
            if(!array_key_exists('denied_domains', $parameters) || ! is_array($parameters['denied_domains'])){
            	$this->addError("Parameter denied_domains should be an array");
            	return false;
            }
            
            if(!array_key_exists('allowed_domains', $parameters) || ! is_array($parameters['allowed_domains'])){
            	$this->addError("Parameter allowed_domains should be an array");
            	return false;
            }
            
            // concat SpiderName with projectName to make it unique
            $parameters['name'] = $this->getName() . "_" . $parameters['name'];
            
            // Link it to the current project
            $parameters["project"] = array(
                "id" => $this->getId()
            );
            
            $newSpider = $this->getScrapySpiderManager()->add($parameters);
            if (! $newSpider) {
                $this->addError($this->getScrapySpiderManager()
                    ->getLastError());
                return false;
            }
            
            return json_decode($newSpider);
        }

        /**
         * Updates a spider in the project
         *
         * @param Array $parameters
         *            Dictionary containing the spider parameters.
         *            Keys:
         *            - id: integer *
         *            - start_domains: array [start_domain_list]
         *            - allowed_domains: array [allowed_domain_regex_list]
         *            - denied_domains: array [denied_domain_regex_list]
         *            - depth: integer
         * @return Boolean
         */
        function editSpider($parameters)
        {
            // we should check that the key name exists
            if (! array_key_exists('id', $parameters) || empty($parameters['id'])) {
                $this->addError("No spider ID specified.");
                return false;
            }
            
            if(!array_key_exists('start_domains', $parameters) || ! is_array($parameters['start_domains'])){
            	$this->addError("Parameter start_domains should be an array");
            	return false;
            }
            
            if(!array_key_exists('denied_domains', $parameters) || ! is_array($parameters['denied_domains'])){
            	$this->addError("Parameter denied_domains should be an array");
            	return false;
            }
            
            if(!array_key_exists('allowed_domains', $parameters) || ! is_array($parameters['allowed_domains'])){
            	$this->addError("Parameter allowed_domains should be an array");
            	return false;
            }
            
            $editedSpider = $this->getScrapySpiderManager()->edit($parameters);
            if (! $editedSpider) {
                $this->addError($this->getScrapySpiderManager()
                    ->getLastError());
                return false;
            }
            
            return json_decode($editedSpider);
        }

        function deleteSpider($id)
        {
            if (! $this->getScrapySpiderManager()->delete($id)) {
                $this->addError($this->getScrapySpiderManager()
                    ->getLastError());
                return false;
            }
            
            return true;
        }
    }
}