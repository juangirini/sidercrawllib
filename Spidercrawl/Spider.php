<?php
if (! class_exists ( 'Spider' )) {
	require_once dirname ( __FILE__ ) . '/SpidercrawlAbstract.php';
	require_once dirname ( __FILE__ ) . '/Spider.php';
	require_once dirname ( __FILE__ ) . '/Job.php';
	require_once dirname ( __FILE__ ) . '/Item.php';
	require_once dirname ( __FILE__ ) . '/../Scrapy/ScrapySpider.php';
	require_once dirname ( __FILE__ ) . '/../Scrapy/ScrapyItem.php';
	require_once dirname ( __FILE__ ) . '/../Scrapy/ScrapyJob.php';
	class Spider extends SpidercrawlAbstract {
		private $scrapySpiderManager;
		private $scrapyItemManager;
		private $scrapyJobManager;
		private $name;
		private $allowed_domains;
		private $denied_domains;
		private $id;
		private $project;
		private $resource_uri;
		private $start_domains;
		private $depth;
		private $items;
		private $meta;
		private $objects;
		private $bot_name;
		private $max_url_crawled;
		private $concurrent_requests;
		private $download_delay;
		private $read_robotstxt;
		private $url_length_limit;
		private $use_proxies;
		
		/**
		 *
		 * @return the $use_proxies
		 */
		public function getUse_proxies() {
			return $this->use_proxies;
		}
		
		/**
		 *
		 * @param field_type $use_proxies        	
		 */
		public function setUse_proxies($use_proxies) {
			$this->use_proxies = $use_proxies;
		}
		
		/**
		 *
		 * @return the $url_length_limit
		 */
		public function getUrl_length_limit() {
			return $this->url_length_limit;
		}
		
		/**
		 *
		 * @param field_type $url_length_limit        	
		 */
		public function setUrl_length_limit($url_length_limit) {
			$this->url_length_limit = $url_length_limit;
		}
		
		/**
		 *
		 * @return the $read_robotstxt
		 */
		public function getRead_robotstxt() {
			return $this->read_robotstxt;
		}
		
		/**
		 *
		 * @param field_type $read_robotstxt        	
		 */
		public function setRead_robotstxt($read_robotstxt) {
			$this->read_robotstxt = $read_robotstxt;
		}
		
		/**
		 *
		 * @return the $download_delay
		 */
		public function getDownload_delay() {
			return $this->download_delay;
		}
		
		/**
		 *
		 * @param field_type $download_delay        	
		 */
		public function setDownload_delay($download_delay) {
			$this->download_delay = $download_delay;
		}
		
		/**
		 *
		 * @return the $concurrent_requests
		 */
		public function getConcurrent_requests() {
			return $this->concurrent_requests;
		}
		
		/**
		 *
		 * @param field_type $concurrent_requests        	
		 */
		public function setConcurrent_requests($concurrent_requests) {
			$this->concurrent_requests = $concurrent_requests;
		}
		public function getMax_url_crawled() {
			return $this->max_url_crawled;
		}
		public function setMax_url_crawled($max_url_crawled) {
			$this->max_url_crawled = $max_url_crawled;
		}
		
		/**
		 *
		 * @param field_type $bot_name        	
		 */
		public function setBot_name($bot_name) {
			$this->bot_name = $bot_name;
		}
		
		/**
		 *
		 * @param field_type $objects        	
		 */
		public function getBot_name() {
			return $this->bot_name;
		}
		
		/**
		 *
		 * @param field_type $objects        	
		 */
		public function setObjects($objects) {
			$this->objects = $objects;
		}
		
		/**
		 *
		 * @param field_type $name        	
		 */
		public function setName($name) {
			$this->name = $name;
		}
		
		/**
		 *
		 * @param field_type $meta        	
		 */
		public function setMeta($meta) {
			$this->meta = $meta;
		}
		
		/**
		 *
		 * @param field_type $allowed_domains        	
		 */
		public function setAllowed_domains($allowed_domains) {
			$this->allowed_domains = $allowed_domains;
		}
		
		/**
		 *
		 * @param field_type $denied_domains        	
		 */
		public function setDenied_domains($denied_domains) {
			$this->denied_domains = $denied_domains;
		}
		
		/**
		 *
		 * @param field_type $id        	
		 */
		public function setId($id) {
			$this->id = $id;
		}
		
		/**
		 *
		 * @param field_type $project        	
		 */
		public function setProject($project) {
			$this->project = $project;
		}
		
		/**
		 *
		 * @param field_type $resource_uri        	
		 */
		public function setResource_uri($resource_uri) {
			$this->resource_uri = $resource_uri;
		}
		
		/**
		 *
		 * @param field_type $start_domains        	
		 */
		public function setStart_domains($start_domains) {
			$this->start_domains = $start_domains;
		}
		
		/**
		 *
		 * @param field_type $depth        	
		 */
		public function setDepth($depth) {
			$this->depth = $depth;
		}
		
		/**
		 *
		 * @param field_type $items        	
		 */
		public function setItems($items) {
		}
		public function getScrapySpiderManager() {
			if (! $this->scrapySpiderManager) {
				$this->scrapySpiderManager = new \ScrapySpider ();
			}
			return $this->scrapySpiderManager;
		}
		public function getScrapyItemManager() {
			if (! $this->scrapyItemManager) {
				$this->scrapyItemManager = new \ScrapyItem ();
			}
			return $this->scrapyItemManager;
		}
		public function getScrapyJobManager() {
			if (! $this->scrapyJobManager) {
				$this->scrapyJobManager = new \ScrapyJob ();
			}
			return $this->scrapyJobManager;
		}
		
		/**
		 * The name consists in a concat btw the projectName and the spiderName.
		 * This method return only the spiderName
		 *
		 * @return the $name
		 */
		public function getName() {
			$names = explode ( '_', $this->name ); // string before underscore should contain projectName, the string after should contain the spiderName
			                                       // this condition is here only to allow backward compatibility for those spider which have been created without the projectName
			if (! array_key_exists ( 1, $names )) {
				return $names [0];
			}
			
			return $names [1];
		}
		
		/**
		 * The name consists in a concat btw the projectName and the spiderName.
		 * This method return the full name concatenated
		 *
		 * @return the $name
		 */
		public function getFullName() {
			return $this->name;
		}
		
		/**
		 *
		 * @return the $allowed_domains
		 */
		public function getAllowed_domains() {
			return $this->allowed_domains;
		}
		
		/**
		 *
		 * @return the $denied_domains
		 */
		public function getDenied_domains() {
			return $this->denied_domains;
		}
		
		/**
		 *
		 * @return the $id
		 */
		public function getId() {
			return $this->id;
		}
		
		/**
		 *
		 * @return the $project
		 */
		public function getProject() {
			return $this->project;
		}
		
		/**
		 *
		 * @return the $resource_uri
		 */
		public function getResource_uri() {
			return $this->resource_uri;
		}
		
		/**
		 *
		 * @return the $start_domains
		 */
		public function getStart_domains() {
			return $this->start_domains;
		}
		
		/**
		 *
		 * @return the $depth
		 */
		public function getDepth() {
			return $this->depth;
		}
		function getItems() {
			$scrapyItems = $this->getScrapyItemManager ()->get ( array (
					'spider' => array (
							'id' => $this->id 
					) 
			) );
			
			if (! $scrapyItems) {
				$this->addError ( $this->getScrapyItemManager ()->getLastError () );
				return false;
			}
			$scrapyItems = json_decode ( $scrapyItems );
			$items = array ();
			foreach ( $scrapyItems->objects as $item ) {
				$items [$item->id] = $this->getItem ( $item );
			}
			return $items;
		}
		function getItem($object) {
			if (isset ( $object->id )) {
				$idItem = $object->id;
			} elseif (is_int ( $object )) {
				$idItem = $object;
			} else {
				throw new \Exception ( 'Unsupported format when trying to get an Item in ' . get_class ( $this ) . '::getItem()' );
			}
			if (! isset ( $this->items [$idItem] )) {
				if (! ($new_item = new Item ( $object ))) {
					
					$this->addError ( "Unable to load the requested Item." );
					return false;
				}
				$this->items [$idItem] = $new_item;
			}
			return $this->items [$idItem];
		}
		
		/**
		 * Add a new item to the spider
		 *
		 * @param Array $parameters
		 *        	Dictionary containing the item parameters.
		 *        	Keys:
		 *        	- name: String *
		 *        	- xPath: String *
		 * @return Boolean
		 */
		function addItem($parameters) {
			// we should check that the key name exists before playing around with it
			if (! array_key_exists ( 'name', $parameters ) || empty ( $parameters ['name'] )) {
				$this->addError ( "No item name specified." );
				return false;
			} elseif (strpos ( $parameters ['name'], "_" ) !== false) { // underscore isa reserved character in names, so it shouldn't be allowed
				$this->addError ( "Underscores are not allowed in the Name field." );
				return false;
			} elseif (strpos ( $parameters ['name'], "-" ) !== false) { // hyphens are not allowed in names
				$this->addError ( "Hyphens are not allowed in the Name field." );
				return false;
			}
			
			// concat FullSpiderName with itemName to make it unique
			$parameters ['name'] = $this->getFullName () . "_" . $parameters ['name'];
			
                        // Link it to the current spider
                        $parameters["spider"] = array(
                            "id" => $this->getId()
                        );
			
			$newItem = $this->getScrapyItemManager ()->add ( $parameters );
			if (! $newItem) {
				$this->addError ( $this->getScrapyItemManager ()->getLastError () );
				return false;
			}
			return json_decode ( $newItem );
		}
		function deleteItem($id) {
			if (! $this->getScrapyItemManager ()->delete ( $id )) {
				$this->addError ( $this->getScrapyItemManager ()->getLastError () );
				return false;
			}
			
			return true;
		}
		public function getJobs($parameters) {
			/*
			 * $parameters = array ( 'project' => array ( 'id' => $this->getProject()->id ) ); if (! is_null ( $idJob )) { $parameters ['job'] = array ( 'id' => $idJob ); } if (! is_null ( $status )) { $parameters ['status'] = $status; }
			 */
			$scrapyJobs = $this->getScrapyJobManager ()->get ( $parameters );
			
			if (! $scrapyJobs) {
				$this->addError ( $this->getScrapyJobManager ()->getLastError () );
				return false;
			}
			$scrapyJobs = json_decode ( $scrapyJobs );
			$jobs = array ();
			foreach ( $scrapyJobs->jobs as $job ) {
				$jobs [$job->id] = $this->getJob ( $job->id );
			}
			return $jobs;
		}
		function getJob($object) {
			// $parameters = array(
			// 'job' => array(
			// 'id' => $idJob
			// ),
			// 'spider' => array(
			// 'id' => $this->getId()
			// )
			// );
			// $newJob = new Job($parameters);
			// @todo:delete this reference to idSpider when Jordi fixes the spider in a job is an ID instead of a name
			/*
			 * if ($newJob->setIdJob($idJob, $this->getId()) === false) { $this->addError("Unable to load the requested Job. " . $this->getScrapyJobManager() ->getLastError()); return false; }
			 */
			// return $newJob;
			if (isset ( $object->id )) {
				$idJob = $object->id;
				$parameters = $object;
			} elseif (is_string ( $object )) {
				$idJob = $object;
				$parameters = array (
						'job' => array (
								'id' => $idJob 
						),
						'project' => array (
								'id' => $this->getProject ()->id 
						) 
				);
			} else {
				throw new \Exception ( 'Unsupported format when trying to get a Job in ' . get_class ( $this ) . '::getJob()' );
			}
			if (! isset ( $this->jobs [$idJob] )) {
				if (! ($new_job = new Job ( $parameters ))) {
					
					$this->addError ( "Unable to load the requested Item." );
					return false;
				}
				$this->jobs [$idJob] = $new_job;
			}
			return $this->jobs [$idJob];
		}
		function getFinishedJobs() {
			$parameters = array (
					'spider' => array (
							'id' => $this->getId () 
					),
					'status' => 'finished' 
			);
			return $this->getJobs ( $parameters );
		}
		function getPendingJobs() {
			$parameters = array (
					'spider' => array (
							'id' => $this->getId () 
					),
					'status' => 'pending' 
			);
			return $this->getJobs ( $parameters );
		}
		function getRunningJob() {
			$parameters = array (
					'spider' => array (
							'id' => $this->getId () 
					),
					'status' => 'running' 
			);
			
			$runningJob = $this->getJobs ( $parameters );
			
			if ((empty ( $runningJob )) || (! $runningJob) || (null === $runningJob)) {
				return false;
			} elseif (sizeof ( $runningJob ) == 1) {
				return array_pop ( $runningJob );
			} elseif (sizeof ( $runningJob ) >= 1) {
				throw new Exception ( "There is more that one Job running for this Spider. You cannot run more than one Job in the same Project at the same time." );
			}
			throw new Exception ( "Unknown error when trying to get running Jobs for this Spider." );
		}
		function getRunningJobInProject() {//alias
                    return $this->getRunningJobsInProject();
                }
		function getRunningJobsInProject() {
			$parameters = array (
					'project' => array (
							'id' => $this->getProject ()->id 
					),
					'status' => 'running' 
			);
			
			$runningJobs = $this->getJobs ( $parameters );
			
			if ((empty ( $runningJobs )) || (! $runningJobs) || (null === $runningJobs)) {
				return false;
			} elseif (sizeof ( $runningJobs ) == 1) {
				return array_pop ( $runningJobs );
			} elseif (sizeof ( $runningJobs ) >= 1) {
				return $runningJobs;
                                //throw new Exception ( "There is more that one Job running for this Project. You cannot run more than one Job in the same Project at the same time." );
			}
			throw new Exception ( "Unknown error when trying to get running Jobs for this Project." );
		}
		function getLastFinishedJob($finishedJobs = null) {
			if (is_null ( $finishedJobs )) {
				if (! ($finishedJobs = $this->getFinishedJobs ())) {
					return false;
				}
			}
			if (! empty ( $finishedJobs )) {
				$lastFinishedJob = false;
				foreach ( $finishedJobs as $job ) {
					if ((! $lastFinishedJob) || ($job->getEnd_time () > $lastFinishedJob->getEnd_time ())) {
						$lastFinishedJob = $job;
					}
				}
				
				return $lastFinishedJob;
			}
			return false;
		}
		
		/**
		 * Starts a new job for the Spider
		 *
		 * @return Object Contains the started job info
		 */
		function startJob() {
			$runningJob = $this->getRunningJobInProject ();
			
			// verify that there is not other job running for this spider
			if ($runningJob) {
				$this->addError ( "There is already a running Job for this Project" );
				return false;
			}

			if (! ($startedJob = $this->getScrapyJobManager ()->start ( $this->getId(), $this->getProject ()->id ))) {
				$this->addError ( "Unable to start a Job. " . $this->getScrapyJobManager ()->getLastError () );
				return false;
			}
			
			return $startedJob;
		}
		
		/**
		 * Cancels a specified job
		 *
		 * @return Object Contains the cancelled job info
		 */
		function cancelJob($idJob) {
			if (! ($cancelledJob = $this->getScrapyJobManager ()->cancel ( $idJob, $this->getProject ()->id ))) {
				$this->addError ( "Unable to cancel the Job. " . $this->getScrapyJobManager ()->getLastError () );
				return false;
			}
			
			return $cancelledJob;
		}
		
		/**
		 * Updates an item in the spider
		 *
		 * @param Array $parameters
		 *        	Dictionary containing the item parameters.
		 *        	Keys:
		 *        	- id: integer *
		 *        	- xPath: String
		 * @return Boolean
		 */
		function editItem($parameters) {
			// we should check that the key name exists
			if (! array_key_exists ( 'id', $parameters ) || empty ( $parameters ['id'] )) {
				$this->addError ( "No item ID specified." );
				return false;
			}
			$editedItem = $this->getScrapyItemManager ()->edit ( $parameters );
			if (! $editedItem) {
				$this->addError ( $this->getScrapyItemManager ()->getLastError () );
				return false;
			}
			
			return json_decode ( $editedItem );
		}
		public function getItemNames() {
			$items = $this->getItems ();
			$itemNames = array ();
			foreach ( $items as $item ) {
				$itemNames [] = $item->getName ();
			}
			return $itemNames;
		}
	}
}