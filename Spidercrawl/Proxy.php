<?php
if (! class_exists('Proxy')) {
    
    require_once dirname(__FILE__) . '/SpidercrawlAbstract.php';
    require_once dirname(__FILE__) . '/../Scrapy/ScrapyProxy.php';

    class Proxy extends SpidercrawlAbstract
    {

        private $scrapyProxyManager;

        private $id;

        private $ip;
        
        private $password;
        
        private $port;

        private $resource_uri;

        private $user;

        /**
         *
         * @param ScrapyProxy $scrapyProxyManager            
         */
        public function setScrapyProxyManager($scrapyProxyManager)
        {
            $this->scrapyProxyManager = $scrapyProxyManager;
        }

        /**
         *
         * @param field_type $id            
         */
        public function setId($id)
        {
            $this->id = $id;
        }

        /**
         *
         * @param field_type $ip            
         */
        public function setIp($ip)
        {
            $this->ip = $ip;
        }

        /**
         *
         * @param field_type $password            
         */
        public function setPassword($password)
        {
            $this->password = $password;
        }

        /**
         *
         * @param field_type $port            
         */
        public function setPort($port)
        {
            $this->port = $port;
        }

        /**
         *
         * @param field_type $resource_uri            
         */
        public function setResource_uri($resource_uri)
        {
            $this->resource_uri = $resource_uri;
        }

        /**
         *
         * @param field_type $user            
         */
        public function setUser($user)
        {
            $this->user = $user;
        }

        private function getScrapyProxyManager()
        {
            if (! $this->scrapyProxyManager) {
                $this->scrapyProxyManager = new ScrapyProxy();
            }
            return $this->scrapyProxyManager;
        }

        /**
         *
         * @return the $id
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         *
         * @return the $ip
         */
        public function getIp()
        {
            return $this->ip;
        }

        /**
         *
         * @return the $port
         */
        public function getPort()
        {
            return $this->port;
        }

        /**
         *
         * @return the $password
         */
        public function getPassword()
        {
            return $this->password;
        }

        /**
         *
         * @return the $resource_uri
         */
        public function getResource_uri()
        {
            return $this->resource_uri;
        }

        /**
         *
         * @return the $user
         */
        public function getUser()
        {
            return $this->user;
        }

    }
}