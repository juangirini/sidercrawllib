<?php

require_once dirname(__FILE__) . '/../Scrapy/ScrapyProject.php';
require_once dirname(__FILE__) . '/../Scrapy/ScrapyProxy.php';
require_once dirname(__FILE__) . '/SpidercrawlAbstract.php';
require_once dirname(__FILE__) . '/Project.php';
require_once dirname(__FILE__) . '/Proxy.php';

class Spidercrawl extends SpidercrawlAbstract
{

    private $scrapyProjectManager;
    private $scrapyProxyManager;

    private $projects = array();
    private $proxies = array();

    function __construct()
    {
    }

    private function getScrapyProjectManager(){
        if(!$this->scrapyProjectManager){
            $this->scrapyProjectManager= new ScrapyProject();
        }
        return $this->scrapyProjectManager;
    }
    
    function getProject($object)
    {
        if(isset($object->id)){
            $idProject = $object->id;
        }elseif(is_int($object)){
            $idProject = $object;
        }else{
            throw new \Exception('Unsupported format when trying to get a Project in '.get_class($this).'::getProject()');
        }
        if (! isset($this->projects[$idProject])) {
            if (!($new_project = new Project($object))){
                
                $this->addError("Unable to load the requested Project.");
                return false;
            }
            $this->projects[$idProject] = $new_project;
        }
        return $this->projects[$idProject];
    }

    function getProjects()
    {
        $scrapyProjects = $this->getScrapyProjectManager()->get();
        if (! $scrapyProjects) {
            $this->addError($this->getScrapyProjectManager()
                ->getLastError());
            return false;
        }
        $scrapyProjects = json_decode($scrapyProjects);
        $projects = array();
        foreach ($scrapyProjects->objects as $project) {
            $projects[$project->id] = $this->getProject($project);
        }
        return $projects;
    }

    /**
     * Add a new project to the system
     *
     * @param Array $parameters
     *            Dictionary containing the project parameters.
     *            Keys:
     *            - name: String *
     * @return Boolean
     */
    function addProject($parameters)
    {
        // we should check that the key name exists before playing around with it
        if (! array_key_exists('name', $parameters) || empty($parameters['name'])) {
            $this->addError("No Project name specified.");
            return false;
        } elseif (strpos($parameters['name'], "_") !== false) { // underscore isa reserved character in names, so it shouldn't be allowed
            $this->addError("Underscores are not allowed in the Name field.");
            return false;
        }elseif (strpos($parameters['name'], "-") !== false) { // hyphens are not allowed in  names
                $this->addError("Hyphens are not allowed in the Name field.");
                return false;
            }
        
        $newProject = $this->getScrapyProjectManager()->add($parameters);
        if (! $newProject) {
            $this->addError($this->getScrapyProjectManager()
                ->getLastError());
            return false;
        }
        
        return json_decode($newProject);
    }

    function deleteProject($id)
    {
        if (! $this->getScrapyProjectManager()->delete($id)) {
            $this->addError($this->getScrapyProjectManager()
                ->getLastError());
            return false;
        }
        
        return true;
    }
    
    
    /***** PROXIES ********/
    
    private function getScrapyProxyManager(){
        if(!$this->scrapyProxyManager){
            $this->scrapyProxyManager= new ScrapyProxy();
        }
        return $this->scrapyProxyManager;
    }
    
    function getProxy($object)
    {
        if(isset($object->id)){
            $idProxy = $object->id;
        }elseif(is_int($object)){
            $idProxy = $object;
        }else{
            throw new \Exception('Unsupported format when trying to get a Proxy in '.get_class($this).'::getProxy()');
        }
        if (! isset($this->proxies[$idProxy])) {
            if (!($new_proxy = new Proxy($object))){
                
                $this->addError("Unable to load the requested Proxy.");
                return false;
            }
            $this->proxies[$idProxy] = $new_proxy;
        }
        return $this->proxies[$idProxy];
    }

    function getProxies()
    {
        $scrapyProxies = $this->getScrapyProxyManager()->get();
        if (! $scrapyProxies) {
            $this->addError($this->getScrapyProxyManager()
                ->getLastError());
            return false;
        }
        $scrapyProxies = json_decode($scrapyProxies);
        $proxies = array();
        foreach ($scrapyProxies->objects as $proxy) {
            $proxies[$proxy->id] = $this->getProxy($proxy);
        }
        return $proxies;
    }

    /**
     * Add a new proxy to the system
     *
     * @param Array $parameters
     *            Dictionary containing the project parameters.
     *            Keys:
     *            - ip: String *
     *            - port: Integer *
     *            - user: String
     *            - password: String
     * @return Boolean
     */
    function addProxy($parameters)
    {
        // we should check that the keys ip and port exist before playing around with it
        if (! array_key_exists('ip', $parameters) || empty($parameters['ip'])) {
            $this->addError("No Proxy ip specified.");
            return false;
        }
        if (! array_key_exists('port', $parameters) || empty($parameters['port'])) {
            $this->addError("No Proxy port specified.");
            return false;
        }
        
        $newProxy = $this->getScrapyProxyManager()->add($parameters);
        if (! $newProxy) {
            $this->addError($this->getScrapyProxyManager()
                ->getLastError());
            return false;
        }
        
        return $newProxy;
    }

    function deleteProxy($id)
    {
        if (! $this->getScrapyProxyManager()->delete($id)) {
            $this->addError($this->getScrapyProxyManager()
                ->getLastError());
            return false;
        }
        
        return true;
    }
            /**
         * Updates a proxy
         *
         * @param Array $parameters
         *            Dictionary containing the proxy parameters.
         *            Keys:
         *            - id: integer *
         *            - ip: String
         *            - port: integer
         *            - user: string
         *            - password: string
         * @return Boolean
         */
        function editProxy($parameters)
        {
            // we should check that the key name exists and is valid
            if (! array_key_exists('id', $parameters) || empty($parameters['id'])) {
                $this->addError("No proxy ID specified.");
                return false;
            }
            
            $editedProxy = $this->getScrapyProxyManager()->edit($parameters);
            if (! $editedProxy) {
                $this->addError($this->getScrapyProxyManager()
                    ->getLastError());
                return false;
            }
            
            return json_decode($editedProxy);
        }
}