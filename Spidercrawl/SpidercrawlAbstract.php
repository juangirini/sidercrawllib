<?php
if (! class_exists('SpidercrawlAbstract')) {

    abstract class SpidercrawlAbstract
    {

        private $errors = array();

        private $scrapyManager;

        /**
         * Polymorphic constructor as getScrapyManager()->get() is a Polymorphic function
         * 
         * @param mixed $object            
         * @return boolean
         */
        function __construct($object=null)
        {
            $scrapyManager = $this->getScrapyManager();
            $scrapyObject = $scrapyManager->get($object);
            
            if (! $scrapyObject) {
                $this->addError($this->getScrapyManager()
                    ->getLastError());
                return false;
            }
            $scrapyObject = json_decode($scrapyObject);

            $vars = get_object_vars($scrapyObject);
            foreach ($vars as $key => $var) {
                $setter = 'set' . ucfirst($key);
                $this->$setter($var);
            }
            unset($scrapyObject);
        }

        function getScrapyManager()
        {
            if (! $this->scrapyManager) {
                $scrapyManager = 'Scrapy' . $this->getClassName();
                $this->scrapyManager = new $scrapyManager();
            }
            return $this->scrapyManager;
        }

        /**
         * Adds an error to the errors stack.
         * 
         * @param String $error
         *            A string containig the error description
         * @return int The new number of elements in the errors stack
         */
        function addError($error)
        {
            return array_push($this->errors, $error);
        }

        /**
         * Gets the las error in the errors stack, and deletes it from the stack.
         * 
         * @return String A string containig the last error in the stack
         */
        function getLastError()
        {
            return array_pop($this->errors);
        }

        /**
         * Gets the entire errors stack and empties it.
         * 
         * @return Array The errors stack
         */
        function getErrors()
        {
            $errors = $this->errors;
            $this->errors = array();
            return $errors;
        }

        function getClassName()
        {
            $class = explode('\\', get_class($this));
            return end($class);
        }
    }
}