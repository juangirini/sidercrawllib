<?php

if (!class_exists('ScrapyAbstract')) {

    require_once dirname(__FILE__) . '/../Services/RestClient.php';

    abstract class ScrapyAbstract {

        private $errors = array();
        private $rest;

        public function getRest() {

            if (!$this->rest) {

                $this->rest = new RestClient(include dirname(__FILE__) . '/../Config/config.php');
            }
            return $this->rest;
        }

        /**
         * Performs GET Request.
         *
         * @param string $method
         *            String that specifies the method path
         * @return Mixed Object or false (Boolean) if it fails.
         */
        function get($method) {
            $rest = $this->getRest();
            $response = $rest->restGet($method);
            // Check the status code
            $status = $response->getStatusCode();
            if ($status != 200) {
                $content = json_decode($response->getContent());
                if (is_null($content)) {
                    $error = is_null($response->getError()) ? "ScrapyAbstract: Unknown error on GET" : $response->getError();
                    $this->addError($error);
                } elseif (property_exists($content, 'error_message')) {
                    $this->addError($content->error_message);
                } else {
                    $this->addError($response->getContent());
                }
                return false;
            }

            return $response->getContent();
        }

        /**
         * Performs POST Request.
         *
         * @param string $method
         *            String that specifies the method path
         * @param Array $parameters
         *            Parameters for the GET request, with
         *            the format: array('parameter'=>'value').
         * @return Boolean
         */
        function post($method, $parameters) {
            // Get the data
            $rest = $this->getRest();

            $response = $rest->restPost($method, json_encode($parameters));

            // Check the status code
            $status = $response->getStatusCode();

            if ($status != 201) {
                $content = json_decode($response->getContent());

                if (is_null($content)) {
                    $error = is_null($response->getError()) ? "ScrapyAbstract: Unknown error on POST" : $response->getError();
                    $this->addError($error);
                } elseif (property_exists($content, 'error_message')) {
                    $this->addError($content->error_message);
                } else {
                    $this->addError($response->getContent());
                }
                return false;
            }
            return $response->getContent();
        }

        /**
         * Performs PUT Request.
         *
         * @param string $method
         *            String that specifies the method path
         * @param Array $parameters
         *            Parameters for the GET request, with
         *            the format: array('parameter'=>'value').
         * @return Boolean
         */
        function put($method, $parameters) {

            // Get the data
            $response = $this->getRest()->restPut($method, json_encode($parameters));

            // Check the status code
            $status = $response->getStatusCode();
            if ($status != 200) {

                $content = json_decode($response->getContent());

                if (is_null($content)) {
                    $error = is_null($response->getError()) ? "ScrapyAbstract: Unknown error on PUT" : $response->getError();
                    $this->addError($error);
                } elseif (property_exists($content, 'error_message')) {
                    $this->addError($content->error_message);
                } else {
                    $this->addError($response->getContent());
                }
                return false;
            }

            return $response->getContent();
        }

        /**
         * Performs DELETE Request.
         *
         * @param string $method
         *            String that specifies the method path
         * @return Boolean
         */
        function delete($method) {
            $response = $this->getRest()->restDelete($method);

            // Check the status code
            $status = $response->getStatusCode();
            if ($status != 204) {
                $content = json_decode($response->getContent());
                $error = is_null($response->getError()) ? "ScrapyAbstract: Unknown error on DELETE" : $response->getError();
                $this->addError((is_null($content) || !property_exists($content, 'error_message')) ? $error : $content->error_message);
                return false;
            }
            return true;
        }

        /**
         * Adds an error to the errors stack.
         *
         * @param String $error
         *            A string containig the error description
         * @return int The new number of elements in the errors stack
         */
        function addError($error) {
            return array_push($this->errors, $error);
        }

        /**
         * Gets the las error in the errors stack, and deletes it from the stack.
         *
         * @return String A string containig the last error in the stack
         */
        function getLastError() {
            return array_pop($this->errors);
        }

        /**
         * Gets the entire errors stack and empties it.
         *
         * @return Array The errors stack
         */
        function getErrors() {
            $errors = $this->errors;
            $this->errors = array();
            return $errors;
        }

        public function getObject($type, $object = null) {
            // if the ID was sent as a stringm then convert to integer
            if (gettype($object) == 'string' && $object == (int) $object) {
                $object = (int) $object;
            }

            $method = 'getObjectBy' . ucfirst(gettype($object));

            if (method_exists($this, $method)) {
                return $this->$method($type, $object);
            } else {
                throw new \Exception(gettype($object) . ' is an unsupported format when trying to get an Object in ' . get_class($this));
            }
        }

        public function getObjectByNULL($type, $null) {
            $method = $type;
            return self::get($method);
        }

        public function getObjectByInteger($type, $id) {
            $method = $type . $id . '/';
            return self::get($method);
        }

        public function getObjectByObject($type, $object) {
            return json_encode($object);
        }

        public function getObjectByArray($type, $array) {
            $method = $type;
            return self::post($method, $array);
        }

    }

}