<?php

class Scrapy
{

    protected $spider = null;

    protected $job = null;

    protected $item = null;
    
    protected $project = null;


    /**
     * Constructor
     *
     * @access public
     * @param
     *            void
     * @return Scrapy
     */
  /*  function __construct()
    {
        $restOptions = array(
            'uri' => self::apiUri,
            'path' => self::apiPath,
            'http_user' => self::http_user,
            'http_pass' => self::http_pass
        );
        
        $this->rest = new RestClient($restOptions);
    }*/

    public function getProject()
    {
        if (is_null($this->project)){
            $this->project = new Project();
        }
        
        return $this->project;
    }

    public function getSpider()
    {
        if (is_null($this->spider))
            $this->spider = new Spider();
        
        return $this->spider;
    }

    public function getItem()
    {
        if (is_null($this->item))
            $this->item = new Item();
        
        return $this->item;
    }

    public function getJob()
    {
        if (is_null($this->job))
            $this->job = new Job();
        
        return $this->job;
    }
}