<?php

// namespace Spidercrawl\Scrapy;
if (!class_exists('ScrapyProxy')) {

    require_once dirname(__FILE__) . '/ScrapyAbstract.php';

    class ScrapyProxy extends ScrapyAbstract {

        /**
         * Polymorphic method that retrieves information about proxies.
         * If $object is null, then it lists all the proxies,
         * otherwise it returns the specified proxy
         *
         * @param Mixed $object
         *            A proxy ID
         * @return Mixed Returns the result as an Object,
         *         or false (Boolean) when it fails.
         */
        function get($object = null) {
            return $this->getObject('proxy/', $object);
        }

        /**
         * Add a new proxy to the system
         *
         * @param Array $parameters
         *            Dictionary containing the proxy parameters.
         *            Keys:
         *            - name: String *
         * @return Boolean
         */
        function add($parameters = array()) {
            if (!is_array($parameters) || empty($parameters)) {
                $this->addError("No proxy parameters specified.");
                return false;
            }

            if (!array_key_exists('ip', $parameters) || empty($parameters['ip'])) {
                $this->addError("No proxy ip specified.");
                return false;
            }

            if (!array_key_exists('port', $parameters) || empty($parameters['port'])) {
                $this->addError("No proxy port specified.");
                return false;
            }

            $method = 'proxy/';
            return parent::post($method, $parameters);
        }

        /**
         * Deletes the specified proxy
         *
         * @param String $id
         *            A proxy ID
         * @return Boolean
         */
        function delete($id = '') {
            if (empty($id)) {
                $this->addError("No proxy ID specified.");
                return false;
            }
            $method = 'proxy/' . $id . '/';
            return parent::delete($method);
        }

        /**
         * Edit a proxy in the system
         *
         * @param Array $parameters
         *        	Dictionary containing the item parameters.
         *        	Keys:
         *        	- id: Integer *
         *              - ip: String
         *              - port: Integer
         *              - user: String
         *              - password: String
         * @return Boolean
         */
        function edit($parameters = array()) {
            if (!is_array($parameters) || empty($parameters)) {
                $this->addError("No proxy parameters specified.");
                return false;
            }

            if (!array_key_exists('id', $parameters) || empty($parameters ['id'])) {
                $this->addError("No proxy ID specified.");
                return false;
            }

            $method = 'proxy/' . $parameters ['id'] . '/';
            unset($parameters ['id']);
            return parent::put($method, $parameters);
        }

    }

}