<?php
if (! class_exists ( 'ScrapySpider' )) {
	
	require_once dirname ( __FILE__ ) . '/ScrapyAbstract.php';
	class ScrapySpider extends ScrapyAbstract {
		
		/**
		 * Retrieve information about spiders.
		 * If $id is null, then it lists all the spiders,
		 * otherwise it returns the specified spider
		 *
		 * @param Array $parameters
		 *        	Dictionary containing the spider parameters.
		 *        	Keys:
		 *        	- project: Array("id"=>project_id)
		 *        	- spider: Array("id"=>spider_id) *
		 * @return Mixed Returns the result as an Object,
		 *         or false (Boolean) when it fails.
		 */
		/*
		 * function get($parameters) { $method = 'spider/'; if (isset($parameters['spider'])&&isset($parameters['spider']['id'])) { $method .= $parameters['spider']['id'] . '/'; } elseif (isset($parameters['project'])&&isset($parameters['project']['id'])) { $method .= '?project=' . $parameters['project']['id']; } return parent::get($method); }
		 */
		
		/**
		 * Polymorphic method that retrieves information about projects.
		 * If $object is null, then it lists all the projects,
		 * otherwise it returns the specified project
		 *
		 * @param Mixed $object
		 *        	A project ID
		 * @return Mixed Returns the result as an Object,
		 *         or false (Boolean) when it fails.
		 */
		function get($object = null) {
			$method = 'spider/';
			if (is_array ( $object )) {
				if (isset ( $object ['spider'] ) && isset ( $object ['spider'] ['id'] )) {
					$method .= $object ['spider'] ['id'] . '/';
					$object = null;
				} elseif (isset ( $object ['project'] ) && isset ( $object ['project'] ['id'] )) {
					$method .= '?project=' . $object ['project'] ['id'];
					$object = null;
				}
			}
			return $this->getObject ( $method, $object );
		}
		
		/**
		 * Add a new spider to the system
		 *
		 * @param Array $parameters
		 *        	Dictionary containing the spider parameters.
		 *        	Keys:
		 *        	- name: String *
		 *        	- project: Array("id"=>project_id) *
		 *        	- start_domains: [start_domain_list] *
		 *        	- allowed_domains: [allowed_domain_regex_list]
		 *        	- denied_domains: [denied_domain_regex_list]
		 *        	- depth: integer
		 * @return Boolean
		 */
		function add($parameters = array()) {
			if (! is_array ( $parameters ) || empty ( $parameters )) {
				$this->addError ( "No spider parameters specified." );
				return false;
			}
			
			if (! array_key_exists ( 'name', $parameters ) || empty ( $parameters ['name'] )) {
				$this->addError ( "No spider name specified." );
				return false;
			}
			if (! array_key_exists ( 'project', $parameters ) || empty ( $parameters ['project'] ) || ! array_key_exists ( 'id', $parameters ['project'] ) || empty ( $parameters ['project'] ['id'] )) {
				$this->addError ( "No spider project specified." );
				return false;
			}
			if (! array_key_exists ( 'start_domains', $parameters ) || empty ( $parameters ['start_domains'] )) {
				$this->addError ( "No spider start domain specified." );
				return false;
			}
			
			$method = 'spider/';
			return parent::post ( $method, $parameters );
		}
		
		/**
		 * Edit a spider in the system
		 *
		 * @param Array $parameters
		 *        	Dictionary containing the spider parameters.
		 *        	Keys:
		 *        	- id: Integer *
		 *        	- start_domains: [start_domain_list]
		 *        	- allowed_domains: [allowed_domain_regex_list]
		 *        	- denied_domains: [denied_domain_regex_list]
		 *        	- depth: integer
		 * @return Boolean
		 */
		function edit($parameters = array()) {
			if (! is_array ( $parameters ) || empty ( $parameters )) {
				$this->addError ( "No spider parameters specified." );
				return false;
			}
			
			if (! array_key_exists ( 'id', $parameters ) || empty ( $parameters ['id'] )) {
				$this->addError ( "No spider ID specified." );
				return false;
			}
			
			$method = 'spider/' . $parameters ['id'] . '/';
			unset ( $parameters ['id'] );
			return parent::put ( $method, $parameters );
		}
		
		/**
		 * Deletes the specified spider
		 *
		 * @param String $id
		 *        	A spider ID
		 * @return Boolean
		 */
		function delete($id = '') {
			if (empty ( $id )) {
				$this->addError ( "No spider ID specified." );
				return false;
			}
			$method = 'spider/' . $id . '/';
			return parent::delete ( $method );
		}
	}
}