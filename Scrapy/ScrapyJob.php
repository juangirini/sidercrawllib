<?php
if (! class_exists('ScrapyJob')) {
    
    require_once dirname(__FILE__) . '/ScrapyAbstract.php';

    class ScrapyJob extends ScrapyAbstract
    {

        /**
         * Retrieve information about jobs from a specified spider
         *
         * @param Array $parameters
         *            Dictionary containing the job parameters.
         *            Keys:
         *            - job: Array("id"=>jobid)
         *            - spider: Array("id"=>spider_id) * / either spider or project are compulsory
         *            - project: Array("id"=>project_id) * /either spider or project are compulsory
         *            - status: String [�finished� | �pending� | �running�]
         * @return Mixed Returns the result as an Object,
         *         or false (Boolean) when it fails.
         */
        function get($filters)
        {
            $method = 'job/';
            return parent::post($method, $filters);
        }


        /**
         * Start a job
         *
         * @param Int $idSpider
         *            A spider ID
         * @param Int $idProject
         *            A project ID
         * @return Boolean
         */
        function start($idSpider = null, $idProject = null)
        {
            if (is_null($idProject)) {
                $this->addError("No project ID specified.");
                return false;
            }
            if (is_null($idSpider)) {
                $this->addError("No spider ID specified.");
                return false;
            }
            
            $parameters = array(
                "project" => array(
                    "id" => $idProject
                ),
                "spider" => array(
                    "id" => $idSpider
                )
            );
            
            $method = 'start_job/';
            return parent::post($method, $parameters);
        }

        /**
         * Cancel a job
         *
         * @param String $idJob
         *            A job ID
         * @param Int $idProject
         *            A project ID
         * @return Boolean
         */
        function cancel($idJob = null, $idProject = null)
        {
            if (is_null($idProject)) {
                $this->addError("No project ID specified.");
                return false;
            }
            if (is_null($idJob)) {
                $this->addError("No job ID specified.");
                return false;
            }
            
            $parameters = array(
                "project" => array(
                    "id" => $idProject
                ),
                "job" => array(
                    "id" => $idJob
                )
            );
            
            $method = 'cancel_job/';
            return parent::post($method, $parameters);
        }

        /**
         * Shows a job's log
         *
         * @param Array $parameters
         *            Dictionary containing the job parameters.
         *            Keys:
         *            - job: Array("id"=>jobid) *
         *            - project: Array("id"=>project_id)
         *            - spider: Array("id"=>spider_id) *
         * @return Boolean
         */
        function log($parameters = array())
        {
            if (! array_key_exists('spider', $parameters) || empty($parameters['spider']) || ! array_key_exists('id', $parameters['spider']) || empty($parameters['spider']['id'])) {
                $this->addError("No spider specified.");
                return false;
            }
            if (! array_key_exists('job', $parameters) || empty($parameters['job']) || ! array_key_exists('id', $parameters['job']) || empty($parameters['job']['id'])) {
                $this->addError("No job specified.");
                return false;
            }
            
            $method = 'log/';
            return parent::post($method, $parameters);
        }

        /**
         * Shows job's results
         *
         * @param Array $parameters
         *            Dictionary containing the job parameters.
         *            Keys:
         *            - spider: Array("id"=>spider_id) *
         *            - job: Array("id"=>jobid)
         *            - url: String
         *            - id: idResult
         * @return Boolean
         */
        function result($parameters = array())
        {
            if (! array_key_exists('spider', $parameters) || empty($parameters['spider']) || ! array_key_exists('id', $parameters['spider']) || empty($parameters['spider']['id'])) {
                $this->addError("No spider specified.");
                return false;
            }
            /* @todo: fix the pagination */
            $method = 'result/?offset=0&limit=999999999';
            return parent::post($method, $parameters);
        }
    }
}