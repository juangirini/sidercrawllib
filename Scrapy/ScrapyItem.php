<?php
if (! class_exists ( 'ScrapyItem' )) {
	
	require_once dirname ( __FILE__ ) . '/ScrapyAbstract.php';
	class ScrapyItem extends ScrapyAbstract {
		
		/**
		 * Retrieve information about items.
		 * If $id is null, then it lists all the items,
		 * otherwise it returns the specified item
		 *
		 * @param String $id
		 *        	An item ID
		 * @param String $idSpider
		 *        	A spider ID
		 * @return Mixed Returns the result as an Object,
		 *         or false (Boolean) when it fails.
		 */
		/*
		 * function get($id = null, $idSpider = null) { $method = 'item'; if (! is_null($id)) { $method .= '/' . $id; } elseif (! is_null($idSpider)) { $method .= '?spider=' . $idSpider; } return parent::get($method); }
		 */
		
		/**
		 * Polymorphic method that retrieves information about items.
		 * If $object is null, then it lists all the items,
		 * otherwise it returns the specified item
		 *
		 * @param Mixed $object
		 *        	A item ID
		 * @return Mixed Returns the result as an Object,
		 *         or false (Boolean) when it fails.
		 */
		function get($object = null) {
			$method = 'item/';
			if (is_array ( $object )) {
				if (isset ( $object ['item'] ) && isset ( $object ['item'] ['id'] )) {
					$method .= $object ['item'] ['id'] . '/';
					$object = null;
				} elseif (isset ( $object ['spider'] ) && isset ( $object ['spider'] ['id'] )) {
					$method .= '?spider=' . $object ['spider'] ['id'];
					$object = null;
				}
			}
			
			return $this->getObject ( $method, $object );
		}
		
		/**
		 * Add a new item to the system
		 *
		 * @param Array $parameters
		 *        	Dictionary containing the item parameters.
		 *        	Keys:
		 *        	- name: String *
		 *        	- spider: Array("id"=>spider_id)
		 *        	- xPath: String (xpath_expression)
		 * @return Boolean
		 */
		function add($parameters = array()) {
			if (! is_array ( $parameters ) || empty ( $parameters )) {
				$this->addError ( "No item parameters specified." );
				return false;
			}
			
			if (! array_key_exists ( 'name', $parameters ) || empty ( $parameters ['name'] )) {
				$this->addError ( "No item name specified." );
				return false;
			}
			
			$method = 'item/';
			return parent::post ( $method, $parameters );
		}
		
		/**
		 * Deletes the specified item
		 *
		 * @param String $id
		 *        	An item ID
		 * @return Boolean
		 */
		function delete($id = '') {
			if (empty ( $id )) {
				$this->addError ( "No item ID specified." );
				return false;
			}
			$method = 'item/' . $id . '/';
			return parent::delete ( $method );
		}
		
		/**
		 * Edit an item in the system
		 *
		 * @param Array $parameters
		 *        	Dictionary containing the item parameters.
		 *        	Keys:
		 *        	- id: Integer *
		 *        	- xPath: String
		 * @return Boolean
		 */
		function edit($parameters = array()) {
			if (! is_array ( $parameters ) || empty ( $parameters )) {
				$this->addError ( "No item parameters specified." );
				return false;
			}
			
			if (! array_key_exists ( 'id', $parameters ) || empty ( $parameters ['id'] )) {
				$this->addError ( "No item ID specified." );
				return false;
			}
			
			$method = 'item/' . $parameters ['id'] . '/';
			unset ( $parameters ['id'] );
			return parent::put ( $method, $parameters );
		}
	}
}