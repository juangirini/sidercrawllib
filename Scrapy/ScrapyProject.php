<?php
// namespace Spidercrawl\Scrapy;
if (! class_exists('ScrapyProject')) {
    
    require_once dirname(__FILE__) . '/ScrapyAbstract.php';

    class ScrapyProject extends ScrapyAbstract
    {

        /**
         * Polymorphic method that retrieves information about projects.
         * If $object is null, then it lists all the projects,
         * otherwise it returns the specified project
         *
         * @param Mixed $object
         *            A project ID
         * @return Mixed Returns the result as an Object,
         *         or false (Boolean) when it fails.
         */
        function get($object = null)
        {
            return $this->getObject('project/',$object);
        }

        /**
         * Add a new project to the system
         *
         * @param Array $parameters
         *            Dictionary containing the project parameters.
         *            Keys:
         *            - name: String *
         * @return Boolean
         */
        function add($parameters = array())
        {
            if (! is_array($parameters) || empty($parameters)) {
                $this->addError("No project parameters specified.");
                return false;
            }
            
            if (! array_key_exists('name', $parameters) || empty($parameters['name'])) {
                $this->addError("No project name specified.");
                return false;
            }
            
            $method = 'project/';
            return parent::post($method, $parameters);
        }

        /**
         * Deletes the specified project
         *
         * @param String $id
         *            A project ID
         * @return Boolean
         */
        function delete($id = '')
        {
            if (empty($id)) {
                $this->addError("No project ID specified.");
                return false;
            }
            $method = 'project/' . $id . '/';
            return parent::delete($method);
        }
    }
}