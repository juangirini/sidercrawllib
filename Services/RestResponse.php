<?php

class RestResponse {

    private $content;
    private $statusCode;
    private $error;

    public function setError($error) {
        $this->error = $error;
    }

    public function getError() {
        return $this->error;
    }

    /**
     * @return the $content
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * @return the $statusCode
     */
    public function getStatusCode() {
        return $this->statusCode;
    }

    /**
     * @param field_type $content
     */
    public function setContent($content) {
        $this->content = $content;
    }

    /**
     * @param field_type $statusCode
     */
    public function setStatusCode($statusCode) {
        $this->statusCode = $statusCode;
    }

}

?>