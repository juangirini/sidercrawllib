<?php

require_once dirname(__FILE__) . '/RestCurlClient.php';
require_once dirname(__FILE__) . '/RestResponse.php';

class RestClient {

    private $restCurlClient;
    private $config;
    private $httpOptions = array();
    
    public function __construct($config) {
        $this->setConfig($config);
        $this->restCurlClient = new RestCurlClient();
        $this->setHttpOptions(array(CURLOPT_USERPWD => $this->getUser() . ':' . $this->getPassword()));
    }

    public function setConfig($config) {
        $this->config = $config;
    }

    public function getConfig() {
        return $this->config;
    }

    public function getRestCurlClient() {
        return $this->restCurlClient;
    }

    public function setHttpOptions($httpOptions) {
        foreach ($httpOptions as $key => $value) {
            $this->httpOptions[$key] = $value;
        }
    }

    public function getHttpOptions() {
        return $this->httpOptions;
    }

    public function restGet($method) {
        $response = new RestResponse();
        $restCurlClient = $this->getRestCurlClient();

        try {
            $response->setContent($restCurlClient
                            ->get($this->getUri() . '/' . $this->getPath() . '/' . $method, $this->getHttpOptions()));
        } catch (Exception $e) {
            $response->setStatusCode($e->getCode());
            $response->setError($e->getMessage());
            return $response;
        }

        $response->setStatusCode($this->getRestCurlClient()->response_info['http_code']);
        return $response;
    }

    public function restDelete($method) {

        $response = new RestResponse();
        $restCurlClient = $this->getRestCurlClient();
        try {
            $response->setContent($restCurlClient
                            ->delete($this->getUri() . '/' . $this->getPath() . '/' . $method, $this->getHttpOptions()));
        } catch (Exception $e) {
            $response->setStatusCode($e->getCode());
            $response->setError($e->getMessage());
            return $response;
        }

        $response->setStatusCode($this->getRestCurlClient()->response_info['http_code']);
        return $response;
    }

    public function restPost($method, $data = null) {

        // Define the header
        $headers = array(
            "Content-Type: application/json"
        );
        $this->setHttpOptions(array(CURLOPT_HTTPHEADER => $headers));
        $response = new RestResponse();
        try {
            $response->setContent($this->getRestCurlClient()
                            ->post($this->getUri() . '/' . $this->getPath() . '/' . $method, $data, $this->getHttpOptions()));
        } catch (Exception $e) {
            $response->setStatusCode($e->getCode());
            $response->setError($e->getMessage());
            return $response;
        }
        $response->setStatusCode($this->getRestCurlClient()->response_info['http_code']);
        return $response;
    }

    public function restPut($method, $data = null) {

        // Define the header
        $headers = array(
            "Content-Type: application/json"
        );
        $this->setHttpOptions(array(CURLOPT_HTTPHEADER => $headers));
        $response = new RestResponse();
        try {
            $response->setContent($this->getRestCurlClient()
                            ->put($this->getUri() . '/' . $this->getPath() . '/' . $method, $data, $this->getHttpOptions()));
        } catch (Exception $e) {
            $response->setStatusCode($e->getCode());
            $response->setError($e->getMessage());
            return $response;
        }
        $response->setStatusCode($this->getRestCurlClient()->response_info['http_code']);
        return $response;
    }

    public function getPath() {
        return $this->config['apiPath'];
    }

    public function getUri() {
        return $this->config['apiUri'];
    }

    public function getUser() {
        return $this->config['http_user'];
    }

    public function getPassword() {
        return $this->config['http_pass'];
    }

}